//电子表格模块

var assert = require('chai').assert;
var should = require('chai').should();
const XLSX = require('../lib/xlsx');

describe('XLSX', function() {
    describe('#read', function() {
        it('读取excel', function() {
            var data = XLSX.read('xlsx/工资表.xlsx');
            assert.isAbove(data.length, 0, '没有读取到数据');
        });
    });

    describe('#save', function() {
        it('读取excel并保存json', function(done) {
            var data = XLSX.read('xlsx/工资表.xlsx');

            XLSX.save(data, (err, newDoc) => {
                //                console.log(newDoc);
                should.not.exist(err);
                should.exist(newDoc);
                done();
            })

            should.exist(data);
        });
    });

    describe('#get', function() {
        it('获取数据表json', function(done) {
            XLSX.get((err, docs) => {
                console.log(docs)
                should.not.exist(err);
                should.exist(docs);
                done();
            });
        });
    });

    describe('#update', function() {
        it('更新数据', function(done) {
            XLSX.update({
                _id: 'QM7PxIPW2uD888hy'
            }, {
                $set: {
                    _state: 'success'
                }
            }, {}, function() {
                done();
            });
        });
    });

});
