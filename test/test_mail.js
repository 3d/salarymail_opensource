//邮件模块

var assert = require('chai').assert;
var should = require('chai').should();
const fs = require('fs');
require.extensions['.html'] = function(module, filename) {
    module.exports = fs.readFileSync(filename, 'utf8');
};
const mail = require('../lib/mail');
var Handlebars = require('handlebars');



describe('MAIL', function () {
    describe('#sendmail', function () {
      var mailContent;
      it('生成邮件内容', function () {
          var html = require('../desktop_client/mailtemplate');
          var template = Handlebars.compile(html);

          var obj = {
                "序号": "10",
                "姓名": "宋吉",
                "岗位": "技术部经理",
                "入职时间": "2016.2.1",
                "工资级别": "年薪",
                "基本工资": " 1,200 ",
                "岗位工资": " 1,600 ",
                "综合补助": " 200 ",
                "特殊补助": " 400 ",
                "部门绩效": " - ",
                "公司奖励": " - ",
                "应发合计": " 3,400 ",
                "考勤扣款": " - ",
                "请假扣款": " - ",
                "其他扣款": " - ",
                "公司缴纳": " 639 ",
                "个人缴纳": " 279 ",
                "实发合计": " 3,121 ",
                "邮箱": "4173639@qq.com"
            };
          var titles = [];
          var cells = [];
          var i = 0;
          for(var key in obj) {
              if (key.startsWith('_') || key ==='createdAt' || key ==='updatedAt')
                continue;

              titles[i] = key;
              cells[i] = obj[key];
              i++;
          }

          var data = {
              'year':2016,
              'month':12,
              'titles':titles,
              'cells': cells
            };

          mailContent = template(data);
      });


        it('发送Mail', function (done) {
            // setup e-mail data with unicode symbols
            var mailOptions = {
                from: '"赌东道 Foo 👥" <wxqxhsw.lj@163.com>', // sender address
                to: '1648049087@qq.com', // list of receivers
                subject: JSON.stringify('你好'), // Subject line
                text: 'Hello 1221夺 🐴', // plaintext body
                html: mailContent // html body
            };

            mail.sendMail(mailOptions, function (error, info) {
                should.not.exist(error);
                should.exist(info);
                if (error) {
                    return console.log(`[error]:${error}`);
                }
                done();
            });
        });
    });


});
